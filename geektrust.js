import debug from 'debug';
import { startApp } from './src/app.js';

const logger = debug('app:index');

(async () => {
  logger('starting app...');
  await startApp();
  logger('app started...');
})();
