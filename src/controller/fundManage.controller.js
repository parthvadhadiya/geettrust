import debug from "debug";
const logger = debug("app:utilsHelper");

import { getOverlap, getArraysIntersectionLength, getStockOfFund } from "../helper/utils.helper.js";
const portfolio = {};

export function createPortfolio({command, stockData }) {
  const funds = command.split(" ").slice(1);
  for (const fund of funds) {
    const obj = getStockOfFund({stockData, fund})
    portfolio[obj.name] = obj.stocks;
  }
}

export function calculateOverlap({command, stockData }) {
  const fund = command.split(" ")[1];
  const newFund = getStockOfFund({stockData, fund})
  if (!newFund) {
    console.log("FUND_NOT_FOUND");
    return;
  }
  for (const portfolioFund in portfolio) {
    const noOfStockInPortfolioFund = portfolio[portfolioFund].length;
    const noOfStockInNewFund = newFund.stocks.length;
    const noOfCommonStock = getArraysIntersectionLength(
      portfolio[portfolioFund],
      newFund.stocks
    );
    const overlapPercentage = getOverlap({
      noOfCommonStock,
      noOfStockInA: noOfStockInPortfolioFund,
      noOfStockInB: noOfStockInNewFund,
    });
    if(overlapPercentage > 0){
      console.log(`${fund} ${portfolioFund} ${overlapPercentage}%`);
    }
  }
}

export function addStock({command, stockData }) {
  const fund = command.split(" ").slice(1, 2)[0];
  const stock = command.split(" ").slice(2).join(" ");
  const newFund = getStockOfFund({stockData, fund})
  if (!newFund) {
    console.log("FUND_NOT_FOUND");
    return;
  }
  portfolio[fund].push(stock);
}

