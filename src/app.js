import debug from "debug";
debug("app:app");
import {
  createPortfolio,
  calculateOverlap,
  addStock,
} from "./controller/fundManage.controller.js";
import { readStockData, readInputCommands } from "./helper/utils.helper.js";
import { enums } from "./constants/enums.js";
const args = process.argv

const inputFilePath = args[2]
const dataFilePath = "data/stock_data.json";

export async function startApp() {
  if(!inputFilePath){
    debug('Please add input file name in command line argument')
  }
  const stockData = await readStockData({ filePath: dataFilePath });
  const commands = await readInputCommands({ filePath: inputFilePath });
  for (const command of commands) {
    const commandTye = command.split(" ")[0];
    if (commandTye === enums.COMMAND_TYPE.CURRENT_PORTFOLIO) {
      createPortfolio({ command, stockData });
    } else if (commandTye === enums.COMMAND_TYPE.CALCULATE_OVERLAP) {
      calculateOverlap({ command, stockData });
    } else if (commandTye === enums.COMMAND_TYPE.ADD_STOCK) {
      addStock({ command, stockData });
    }
  }
}
