import { readStockData, readInputCommands } from "./../helper/utils.helper.js";
const args = process.argv;

const inputFilesPath = ["test1.txt"];
const dataFilePath = "data/stock_data.json";

const stockData = await readStockData({ filePath: dataFilePath });
const commands = await readInputCommands({ filePath: inputFilePath[0] });
describe("portfolio overlap", function () {
  it("check overlap", function () {
    const stockData = await readStockData({ filePath: dataFilePath });
    const commands = await readInputCommands({ filePath: inputFilePath });
    for (const command of commands) {
      const commandTye = command.split(" ")[0];
      if (commandTye === enums.COMMAND_TYPE.CURRENT_PORTFOLIO) {
        createPortfolio({ command, stockData });
      } else if (commandTye === enums.COMMAND_TYPE.CALCULATE_OVERLAP) {
        calculateOverlap({ command, stockData });
      } else if (commandTye === enums.COMMAND_TYPE.ADD_STOCK) {
        addStock({ command, stockData });
      }
    }
  });
});
