import debug from "debug";
import fs from "fs";

const logger = debug("app:utilsHelper");


export function readInputCommands({ filePath }) {
  const inputData = fs.readFileSync(filePath, { encoding: "utf8", flag: "r" });
  return inputData.split("\n");
}

export function getStockOfFund({stockData, fund}){
  return stockData.funds.find((o) => o.name === fund);
}

export function readStockData({ filePath }) {
  const file = fs.readFileSync(filePath, "utf8");
  try {
    const data = JSON.parse(file);
    return data;
  } catch (err) {
    logger.error(`Error parsing JSON string: ${err}`);
  }
}

export function getOverlap({
  noOfCommonStock,
  noOfStockInA,
  noOfStockInB,
}) {
  const overlap = ((2 * noOfCommonStock) / (noOfStockInA + noOfStockInB)) * 100;
  return overlap.toFixed(2);
}

export function getArraysIntersectionLength(a1, a2) {
  return a1.filter(function (n) {
    return a2.indexOf(n) !== -1;
  }).length;
}
